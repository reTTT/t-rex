extends Control

var cfg

func _ready():
	cfg = $"/root/config"
	$btn_sfx.pressed = !cfg.get_value("sound", true)
	_on_btn_sfx_toggled($btn_sfx.pressed)

func _on_btn_sfx_toggled(button_pressed):
	var idx = AudioServer.get_bus_index("sfx")
	AudioServer.set_bus_mute(idx, !button_pressed)
	cfg.set_value("sound", button_pressed)

