extends Node

signal change_score(hi_score, score)

var score : float = 0.0
var hi_score : int = 0
var cfg


func _ready():
	cfg = $"/root/config"
	hi_score = cfg.get_value("hi_score", 0)
	reset()

func add_score(score):
	var old = self.score
	self.score += score

	if floor(old) < floor(self.score):
		emit_signal("change_score", hi_score, get_score())

func reset():
	if score > hi_score:
		hi_score = int(score)
		cfg.set_value("hi_score", hi_score)

	score = 0
	emit_signal("change_score", hi_score, int(score))


func get_score() -> int:
	return int(score)
	
func get_hi_score() -> int:
	return hi_score

