extends Node

onready var game = get_parent()
onready var game_control = $"/root/game_control"

func _ready():
	if OS.get_name() == "Android" || OS.get_name() == "IOS":
		set_process_unhandled_input(false)

func _unhandled_input(ev):
	var state = game_control.get_state()
	
	if ev is InputEventMouseButton:
		match state:
			game_control.GameState.Menu:
				print(str(ev.button_index))
				if !ev.is_pressed() && !ev.is_echo() && ev.button_index == BUTTON_LEFT:
					game_control.change_state(game_control.GameState.Play)
			game_control.GameState.Play:
				if ev.is_pressed() && ev.button_index == BUTTON_LEFT:
					game.jump()
	elif ev is InputEventKey:
		match state:
			game_control.GameState.Menu:
				if !ev.is_pressed() && !ev.is_echo():
					game_control.change_state(game_control.GameState.Play)
			game_control.GameState.Play:
				if ev.is_pressed() && (ev.scancode ==  KEY_SPACE || ev.scancode == KEY_UP):
					game.jump()
				elif ev.is_pressed() && ev.scancode == KEY_DOWN && !ev.is_echo():
					game.crouch(true)
				elif !ev.is_pressed() && ev.scancode == KEY_DOWN && !ev.is_echo():
					game.crouch(false)
			game_control.GameState.GameOver:
				if !ev.is_pressed() && !ev.is_echo():
					game.reset()
