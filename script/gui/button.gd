class_name ScaledButton
tool
extends Control

signal pressed
signal released

export var text = "" setget set_text, get_text

export (StyleBoxTexture) var normal
export (StyleBoxTexture) var disable
#export (Color) var modulate = Color(1, 1, 1, 1)

export (Font) var font
export (Color) var font_color = Color(1, 1, 1, 1)

export (bool) var disabled = false

var pressed = false
var hover = false

var glow = 0.05

func set_text(ntext):
	text = ntext
	update()

func get_text():
	return text

func is_disabled():
	return disabled
	
func is_pressed():
	return pressed && !disabled

func set_disabled(disabled):
	self.disabled = disabled
	if self.disabled:
		pressed = false
	update()

func _ready():
	update()

func _calc_text_rect(rect):
	var trect = rect
	if !normal:
		return trect

	var l = normal.get_margin_size(0)
	var r = normal.get_margin_size(1)
	var t = normal.get_margin_size(2)
	var b = normal.get_margin_size(3)
	trect.position += Vector2(l,t)
	trect.size -= Vector2(l+r, t+b)
	return trect

func _dec_rect(rect, percent):
	var r = rect
	r.position += r.size * (percent / 2)
	r.size -= r.size * percent
	return r

func _draw():
	var rect = Rect2(0, 0, get_size().x, get_size().y)
	
	if is_pressed():
		draw_set_transform(rect.size * glow, 0, Vector2(1.0 - glow*2, 1.0 - glow*2))
	elif hover:
		draw_set_transform(rect.size * -glow, 0, Vector2(1.0 + glow*2, 1.0 + glow*2))

	var box = normal

	if disabled && disable:
		box = disable

	if box:
		draw_style_box(box, rect)
	
	if font && !text.empty():
		var r = _calc_text_rect(rect)
		var tsize = font.get_string_size(text)
		var pos = r.position + Vector2((r.size.x - tsize.x) / 2,  (r.size.y - tsize.y) / 2 + tsize.y - font.get_descent())
		draw_string(font, pos, text, font_color)

func _gui_input(e):
	if is_disabled():
		return
	
	if e is InputEventMouseButton:
		if !pressed:
			if e.button_index == BUTTON_LEFT && e.pressed:
				pressed = true
				update()
				emit_signal("pressed")
				self.accept_event()
#				print("pressed")
		else:
			if e.button_index == BUTTON_LEFT && !e.pressed:
				pressed = false
				update()
				emit_signal("released")
				self.accept_event()
#				print("released")
	elif e is InputEventMouseMotion && pressed:
		var r = get_rect()
		r.position = Vector2(0, 0)
		var last = pressed
		pressed = r.has_point(e.position)
		if last != pressed:
			update()
			self.accept_event()

func _notification(what):
	if (what==NOTIFICATION_MOUSE_ENTER):
		update()
#		hover = true
#		print("NOTIFICATION_MOUSE_ENTER")
	elif (what == NOTIFICATION_MOUSE_EXIT):
		update()
#		hover = false
#		print("NOTIFICATION_MOUSE_EXIT")
	elif (what == NOTIFICATION_FOCUS_ENTER):
		update()
#		print("NOTIFICATION_FOCUS_ENTER")
	elif (what == NOTIFICATION_FOCUS_EXIT):
		update()
#		print("NOTIFICATION_FOCUS_EXIT")
	elif (what == NOTIFICATION_THEME_CHANGED):
		update()
#		print("NOTIFICATION_THEME_CHANGED")
	elif (what == NOTIFICATION_VISIBILITY_CHANGED):
		update()
#		print("NOTIFICATION_VISIBILITY_CHANGED")
	elif (what == NOTIFICATION_RESIZED):
		update()
#		print("NOTIFICATION_RESIZED")
#	elif (what == NOTIFICATION_MODAL_CLOSED):
#		update()
