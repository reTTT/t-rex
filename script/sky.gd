extends Polygon2D

const star_inst = preload("res://prefab/star.tscn")

var top_colors = [Color(0x15092Aff), Color(0x6C6480ff), Color(0x89B9D0ff)]
var btm_colors = [Color(0x303E57ff), Color(0xAC7963ff), Color(0xCAD7DBff)]

#var night_colors = [Color(0x15092Aff), Color(0x6C6480ff), Color(0xffffffff)]
var night_colors = [Color(0x89B9D0ff), Color(0x6C6480ff), Color(0xffffffff)]

var seq = [0, 0, 1, 2, 2, 2, 1, 0, 0];

var ORBITS_RADIUS = 400
var STARS_HEIGHT = 500
var colWhite = Color(1, 1, 1, 1)

export (int) var num_stars : int = 60

func _create_stars():
	var width = $"/root/global_params".get_game_width()
	var height = get_viewport_rect().size.y / 2

	for i in range(num_stars):
		var star = star_inst.instance()
		star.name = "star_%d" % i
		
		star.position = Vector2(rand_range(width/-2, width/2), rand_range(height/-2, height/2))
		var star_scale = rand_range(0.4, 1.2)
		star.scale = Vector2(star_scale, star_scale)
		star.self_modulate.a = 0
		
		$stars.add_child(star)


func _ready():
	ORBITS_RADIUS = get_viewport_rect().size.x / 2
	STARS_HEIGHT = get_viewport_rect().size.y / 2
	_create_stars()
	_apply()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(dt):
	_apply()


func _apply():
	var time = $"/root/time_control".get_time()
	var seq_id : int = int(time / 3)
	var seq_residue = time / 3 - seq_id;
	var zenith = -(time / 12.0 * PI - (PI / 2))
	
	sky_update(seq_id, seq_residue, zenith)
	sun_update(time, seq_id, seq_residue, zenith)
	moon_update(time, seq_id, seq_residue, zenith)
	stars_update(time, seq_id, seq_residue, zenith)
	
	var col1 = night_colors[seq[seq_id]]
	var col2 = night_colors[seq[seq_id + 1]]
	var colSkyBtm = col1.linear_interpolate(col2, seq_residue)
	$cm.color = colSkyBtm


func sky_update(seq_id, seq_residue, zenith):
	var col1 = top_colors[seq[seq_id]]
	var col2 = top_colors[seq[seq_id + 1]]
	var colSkyTop = col1.linear_interpolate(col2, seq_residue)

	col1 = btm_colors[seq[seq_id]]
	col2 = btm_colors[seq[seq_id + 1]]
	var colSkyBtm = col1.linear_interpolate(col2, seq_residue)
	
	vertex_colors[0] = colSkyTop
	vertex_colors[1] = colSkyTop
	vertex_colors[2] = colSkyBtm
	vertex_colors[3] = colSkyBtm


func sun_update(time, seq_id, seq_residue, zenith):
	 # Calculate sun position, scale and colors
	var a = 0
	var colSunGlow
	
	if (seq_id == 2):
		a = sin(seq_residue * (PI / 2))
	elif (seq_id == 5):
		a = cos(seq_residue * (PI / 2))
	elif (seq_id > 2 && seq_id < 5):
		a = 1.0
	else:
		a = 0.0

	var colSun = Color(0xEAE1BEFF)
	colSun = colSun * (1 - a) + colWhite * a;
	a = (cos(time / 6.0 * PI) + 1.0) / 2.0
	if seq_id >= 2 && seq_id <= 6:
		colSunGlow = colWhite * a
		colSunGlow.a = 1.0
	else:
		colSunGlow = Color(0x000000FF)

	var pos = $anchor.position

	var sunX = pos.x + cos(zenith) * ORBITS_RADIUS
	var sunY = pos.y * 1.2 + sin(zenith) * ORBITS_RADIUS
	var sunS = 1.0 - 0.3 * sin((time - 6.0) / 12.0 * PI)

	var sunGlowS = 3.0 * (1.0 - a) + 3.0
	
	$sun.position = Vector2(sunX, sunY)
	
	
	$sun/gfx.self_modulate = colSun
	$sun/gfx.scale = Vector2(sunS, sunS)
	
	$sun/glow.self_modulate = colSunGlow
	$sun/glow.scale = Vector2(sunGlowS, sunGlowS)
	
#	$sun/glow.color = colSunGlow
#	$sun/glow.texture_scale = sunGlowS


func moon_update(time, seq_id, seq_residue, zenith):
	var a = 0.0
	
	if seq_id >= 6:
		a = sin((time - 18.0) / 6.0 * (PI / 2))
	else:
		a = sin((1.0 - time / 6.0) * (PI / 2))
	
	var col_moon = Color(0xFFFFFF20)
	col_moon = col_moon * (1 - a) + colWhite * a
	var col_glow = colWhite
	col_glow.a = 0.5 * a

	var pos = $anchor.position
	
	var base_scale = 2.0
	
	pos.x = pos.x + cos(zenith - PI) * ORBITS_RADIUS
	pos.y = pos.y * 1.2 + sin(zenith - PI) * ORBITS_RADIUS
	var moon_scale = base_scale - 0.3 * sin((time + 6.0) / 12.0 * PI)
	var glow_scale = a * 0.4 + 0.5 * base_scale;
#	var glow_scale = a * 1.0 + 0.5;
	
	$moon.position = pos
	
	$moon/gfx.self_modulate = col_moon
	$moon/gfx.scale = Vector2(moon_scale, moon_scale)
	
	$moon/glow.self_modulate = col_glow
	$moon/glow.scale = Vector2(glow_scale, glow_scale)
	
#	$moon/glow.color = col_glow
#	$moon/glow.texture_scale = glow_scale

func stars_update(time, seq_id, seq_residue, zenith):
	if seq_id >= 6 || seq_id < 2:
		for star in $stars.get_children():
			var a = 1.0 - star.position.y / STARS_HEIGHT
			a *= rand_range(0.6, 1.0)

			if seq_id >= 6:
				 a *= sin((time - 18.0) / 6.0 * (PI / 2))
			else:
				a *= sin((1.0 - time / 6.0) * (PI / 2))

			var col = star.self_modulate
			col.a = a
			star.self_modulate = col
			
