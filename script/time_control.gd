extends Node

var real_time : bool = false
var time : float = 0.0
var speed : float = 0.2

func _ready():
	randomize()
	time = rand_range(0, 24)

func _process(dt):
	time += speed * dt
	if time > 24.0:
		time -= 24.0
	elif time < 0.0:
		time += 24.0

func get_time():
	if real_time:
		return OS.get_time()["hour"]
	return time
