extends Node2D

var idx_seed : int = 0
var elapsed_time = 2
var width = 0.0

var game_control

var objects_inst = [
	{
		"chance": 1,
		"pos_y": [0],
		"inst": preload("res://prefab/cactus_b1.tscn"),
	},
	{
		"chance": 1,
		"pos_y": [0],
		"inst": preload("res://prefab/cactus_b2.tscn"),
	},
	{
		"chance": 1,
		"pos_y": [0],
		"inst": preload("res://prefab/cactus_b4.tscn"),
	},
#	{
#		"chance": .2,
#		"pos_y": [0],
#		"inst": preload("res://prefab/boost.tscn"),
#	},
	{
		"chance": .5,
		"pos_y": [-50, -30],
		"inst": preload("res://prefab/pterodactyl.tscn"),
	},
]


func _get_rnd_inst():
	var full_chance = 0
	
	for obj in  objects_inst:
		full_chance += obj.chance
		
	var chance = rand_range(0, full_chance)
	
	var cur_chance = 0
	for obj in  objects_inst:
		cur_chance += obj.chance
		if cur_chance >= chance:
			return obj
	
	return null


func get_idx():
	idx_seed += 1
	return idx_seed


func _create():
	var obj_inst = _get_rnd_inst()
	var obj = obj_inst.inst.instance()
	
	obj.position = Vector2(width/2, obj_inst.pos_y[randi() % obj_inst.pos_y.size()])
	obj.name = "%s_%d" % [obj.name, get_idx()]
	add_child(obj)

	
func reset():
	elapsed_time = 2
	_clear()

	
func _clear():
	for c in get_children():
		c.queue_free()


func _ready():
	game_control = $"/root/game_control"
	width = $"/root/global_params".get_game_width()
	pass # Replace with function body.


func _get_rnd_delay(percent):
	var left = 0.5
	var delay = 2.0 * (1.0 - clamp(percent, 0.0, 1.0))
	var right = left + delay  
	return rand_range(left, right)


func _process(dt):
	var speed = game_control.get_speed()
	if speed <= 0:
		return
	
	var percent = game_control.get_speed_percent()
	
	elapsed_time -= dt
	if elapsed_time <= 0:
		_create()
		elapsed_time = _get_rnd_delay(percent)
		
	for c in get_children():
		c.position -= Vector2(speed*dt, 0)
		if c.position.x < (width/-2):
			c.queue_free()

