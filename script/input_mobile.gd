extends Node

onready var game = get_parent()
onready var game_control = $"/root/game_control"

var idx = -1
var elapsed_time = 0.0
var drag = false

func _ready():
	game_control.connect("change_state", self, "_on_change_state") 
	if OS.get_name() != "Android" && OS.get_name() != "IOS":
		set_process(false)
		set_process_unhandled_input(false)


func _on_change_state(state):
	if state == game_control.GameState.GameOver || state == game_control.GameState.Menu:
		elapsed_time = 0.0
		drag = false
		idx = -1

func _process(dt):
	if idx == -1:
		return
		
	elapsed_time += dt
		
	if !drag:
		if elapsed_time > 0.10:
			game.jump()

#	if  drag && game.tirex.state == game.tirex.TirexState.Crouch:
#		game.crouch(false)

func _unhandled_input(ev):

	var state = game_control.get_state()
	
	if ev is InputEventScreenTouch:
		match state:
			game_control.GameState.Menu:
				if !ev.is_pressed() && !ev.is_echo():
					game_control.change_state(game_control.GameState.Play)
			game_control.GameState.Play:
				if ev.is_pressed() && idx == -1:
					idx = ev.index
				elif !ev.is_pressed() && idx == ev.index:
					if drag:
						game.crouch(false)
					elif elapsed_time < 0.1:
						game.jump()
					
					idx = -1
					drag = false
					elapsed_time = 0.0
					
	elif ev is InputEventScreenDrag:
		match state:
			game_control.GameState.Play:
				#$"/root/game/ui/help".text = "idx: " + str(idx) + " drag: " + str(ev.index)
				if ev.index == idx && ev.relative.length() > 1 && abs(ev.relative.y) > abs(ev.relative.x) && ev.relative.y > 0:
					drag = true
					game.crouch(true)
