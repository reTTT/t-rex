extends Node

var tirex_pos
var tirex
var jumped = false

var scores
var game_control

func _on_change_state(state):
	match state:
		game_control.GameState.Menu:
			tirex.change_state(tirex.TirexState.Idle)
			$"dlg/main".visible = true
			$"dlg/game_over".visible = false
		game_control.GameState.Play:
			$"dlg/main".visible = false
			jumped = false
			tirex.change_state(tirex.TirexState.Run)
		game_control.GameState.GameOver:
			$tween.stop_all()
			tirex.change_state(tirex.TirexState.Dead)
			$"dlg/game_over".visible = true


func _ready():
	game_control = $"/root/game_control"
	game_control.connect("change_state", self, "_on_change_state")
	scores = $"/root/scores"
	
	tirex = $"2d/tirex"
	tirex_pos = tirex.position
	tirex.connect("area_entered", self, "_on_tirex_colision")
	
	_on_change_state(game_control.get_state())
	


func _process(dt):
	var speed = game_control.get_speed()

	if speed <= 0:
		return

	scores.add_score((speed * dt) / 100)


func jump():
	if jumped || !tirex.can_jamp():
		return
	
	jumped = true
	var ani_len_half = .3
	var h = -220
	
	$tween.interpolate_callback(tirex, 0, "change_state", tirex.TirexState.Jump)
	$tween.interpolate_method(tirex, "set_position", tirex.position, tirex.position + Vector2(0, h), ani_len_half, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	$tween.interpolate_callback(tirex, ani_len_half*2, "change_state", tirex.TirexState.Run)
	$tween.interpolate_method(tirex, "set_position", tirex.position + Vector2(0, h), tirex.position, ani_len_half, Tween.TRANS_LINEAR, Tween.EASE_IN, ani_len_half)
	$tween.interpolate_callback(self, ani_len_half*2, "_on_finish_jump")
	$tween.start()
	
func _on_finish_jump():
	jumped = false


func crouch(enable):
	if jumped:
		return
	
	if enable:
		tirex.change_state(tirex.TirexState.Crouch)
	else:
		tirex.change_state(tirex.TirexState.Run)

func reset():
	tirex.position = tirex_pos
	$"2d/bkg/generator".reset()
	scores.reset()
	game_control.change_state(game_control.GameState.Menu)

func _on_tirex_colision(area):
#	return
	if area is Boost:
		tirex.boost_start(area.life_time)
	elif !tirex.is_imortal():
		game_control.change_state(game_control.GameState.GameOver)

func _on_reset_pressed():
	reset()
