extends Polygon2D


var game_control

export var speed_scale:float = 1.0


# Called when the node enters the scene tree for the first time.
func _ready():
	game_control = $"/root/game_control"
	pass # Replace with function body.


func _process(dt):
	var speed = game_control.get_speed()
	var coord = uv
	for i in range(coord.size()):
		coord[i].x += speed * speed_scale * dt
	uv = coord
