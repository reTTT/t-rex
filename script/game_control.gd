extends Node

enum GameState {
	Menu,
	Play,
	GameOver
}

signal change_state(state)

var state = GameState.Menu
var min_speed : float = 900
var max_speed : float = 1500 

var scores

# Called when the node enters the scene tree for the first time.
func _ready():
	scores = $"/root/scores"
	pass # Replace with function body.


func change_state(state):
	if self.state != state:
		self.state = state
		emit_signal("change_state", self.state)


func get_state():
	return state


func get_speed() -> float:
	if state != GameState.Play:
		return 0.0

	return min(min_speed + scores.get_score() / 100 * 50, max_speed)


func get_speed_percent() -> float:
	var speed = get_speed()
	if speed == 0.0:
		return 0.0
	return 1.0 / (max_speed - min_speed) * (speed - min_speed)
