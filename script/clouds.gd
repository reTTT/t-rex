extends Node2D

const cloud_inst = preload("res://prefab/cloud.tscn")
var width = 0.0
var height = 0.0
var game_control
var speed = 100

func _create_cloud(pos):
	var cloud = cloud_inst.instance()
	cloud.position = pos
	var cloud_scale = rand_range(0.8, 1.8)
	cloud.scale = Vector2(cloud_scale, cloud_scale)
	cloud.flip_h = randf() > 0.5
	add_child(cloud)

func _create_start():
	var pos_x = width / -2.0  
	
	while pos_x < width / 2:
		var x = rand_range(width*0.1, width*0.15)
		var pos = Vector2(pos_x+x, rand_range(-height/2, height/4))
		_create_cloud(pos)
		pos_x += x

func _ready():
	game_control = $"/root/game_control"
	width = $"/root/global_params".get_game_width()
	height = get_viewport_rect().size.y / 4
	_create_start()
	pass # Replace with function body.

func _process(dt):
	var cur_speed = game_control.get_speed() + speed
	update_cloud(cur_speed, dt)

func update_cloud(speed, dt):
	for cloud in get_children():
		cloud.position += Vector2(speed / 8 * dt * -1, 0)
		if cloud.position.x < width / -2.0:
			cloud.position = Vector2(width / 2.0, rand_range(-height/2, height/4))


