extends Label

var delay : float = 1.0
var elapsed_time : float = 0.0

func _ready():
	_update_monitor()
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	elapsed_time += delta
	if elapsed_time >= delay:
		elapsed_time = 0.0
		_update_monitor()
		
func _update_monitor():
	text = "fps: %d" % Performance.get_monitor(Performance.TIME_FPS)
