extends Camera2D


export var zoom_portrait:float = 1.2
export var zoom_landscape:float = 0.9

var tween

# Called when the node enters the scene tree for the first time.
func _ready():
	tween = Tween.new()
	tween.name = "tween"
	add_child(tween)
	
	get_viewport().connect("size_changed", self, "_on_size_changet")
	_on_size_changet()
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass



func _on_size_changet():
	var new_zoom = Vector2(zoom_portrait, zoom_portrait)
	if get_viewport_rect().size.x > get_viewport_rect().size.y:
		new_zoom = Vector2(zoom_landscape, zoom_landscape)

	if zoom != new_zoom:
		tween.interpolate_method(self, "set_zoom", zoom, new_zoom, 0.25, Tween.TRANS_LINEAR, Tween.EASE_OUT)
		tween.start()
	
