extends Control

func _on_change_score(hi : int, score : int):
	$hi.text = "HI: %05d" % [hi]
	$score.text = "%05d" % [score]

func _ready():
	var scores = $"/root/scores"
	scores.connect("change_score", self, "_on_change_score")
	_on_change_score(scores.get_hi_score(), scores.get_score())

