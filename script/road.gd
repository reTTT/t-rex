extends Sprite

var game_control

export var speed_scale:float = 1.0

func _ready():
	game_control = $"/root/game_control"
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(dt):
	var speed = game_control.get_speed()
	self.region_rect.position.x += speed * speed_scale * dt
