extends Node

var map = {}
var need_save = false
const fname = "user://cfg.dat"


func _ready():
	load_cfg()

func _exit_tree():
	save_cfg()

func set_value(key, val):
	if key != null:
		map[key] = val
		need_save = true

func get_value(key, def_val = null):
	if key != null && def_val != null && !map.has(key):
		set_value(key, def_val)
		return def_val
	
	return map.get(key, def_val)

func has_key(key):
	return map.has(key)

func save_cfg():
	if !need_save:
		return

	var file = File.new()
	if file.open(fname, File.WRITE) != OK:
		return

	var data = to_json(map)
	file.store_string(data)
	file.close()

	need_save = false

func load_cfg():
	var file = File.new()
	if file.open(fname, File.READ) != OK:
		return

	var content = file.get_as_text()
	file.close()
	map = parse_json(content)
	need_save = false
